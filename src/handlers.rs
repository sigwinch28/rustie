use irc::{Command, Privmsg, Channel};

use std::collections::LinkedList;

#[derive(Debug)]
pub enum Action {
    Reply(Command),
    ReplyMsg(Privmsg),
}

impl Action {
    pub fn to_command(self) -> Command {
        match self {
            Action::Reply(c) => c,
            Action::ReplyMsg(m) => m.to_command(),
        }
    }
}

pub type Actions = Vec<Action>;

pub trait Handler {
    fn handle_connect(&mut self) -> Actions;

    fn handle_join(&mut self, &Channel) -> Actions;

    fn handle_command(&mut self, &Command) -> Actions;
}

pub struct Dispatcher<'a> {
    handlers: LinkedList<&'a mut Handler>,
}

impl<'a> Dispatcher<'a> {
    pub fn new() -> Dispatcher<'a> {
        Dispatcher {
            handlers: LinkedList::new(),
        }
    }

    pub fn register(&mut self, handler: &'a mut Handler) -> () {
        self.handlers.push_back(handler)
    }
}


impl<'a> Handler for Dispatcher<'a> {
    fn handle_connect(&mut self) -> Actions {
        self.handlers.iter_mut().map(|h| h.handle_connect()).flatten().collect()
    }

    fn handle_join(&mut self, channel: &Channel) -> Actions {
        self.handlers.iter_mut().map(|h| h.handle_join(channel)).flatten().collect()
    }

    fn handle_command(&mut self, command: &Command) -> Actions {
        self.handlers.iter_mut().map(|h| h.handle_command(command)).flatten().collect()
    }
   
}


pub struct Ping {}

impl Ping {
    pub fn new() -> Ping {
        Ping{}
    }
}

impl Handler for Ping {
    fn handle_connect(&mut self) -> Actions {
        vec![]
    }

    fn handle_join(&mut self, _channel: &Channel) -> Actions {
        vec![]
    }
    
    fn handle_command(&mut self, command: &Command) -> Actions {
        if command.name == "PING" {
            vec![Action::Reply(Command{
                prefix: command.prefix.as_ref().map(|s| s.to_string()),
                name: "PONG".to_string(),
                args: command.args.clone(),
            })]
        } else {
            vec![]
        }
    }
}

pub struct LoggerMiddleware<T: Handler> {
    child: T,
}

impl<T: Handler> LoggerMiddleware<T> {
    pub fn new(child: T) -> LoggerMiddleware<T> {
        LoggerMiddleware{ child: child }
    }

    fn log_action(action: &Action) {
        let cmd : Command;
        let arg = match action {
            Action::Reply(c) => c,
            Action::ReplyMsg(m) => {
                cmd = m.to_command();
                &cmd
            }
        };
        println!("[server <- client] {}", arg);
    }

}

impl<T: Handler> Handler for LoggerMiddleware<T> {
    fn handle_connect(&mut self) -> Actions {
        println!("[connected]");
        
        let actions = self.child.handle_connect();
        for action in actions.iter() {
            LoggerMiddleware::<T>::log_action(&action);
        }
        actions
    }

    fn handle_join(&mut self, channel: &Channel) -> Actions {
        println!("[join] {}", channel);

        let actions = self.child.handle_join(channel);
        for action in actions.iter() {
            LoggerMiddleware::<T>::log_action(&action)
        }
        actions
    }

    fn handle_command(&mut self, command: &Command) -> Actions {
        println!("[server -> client] {}", command);

        let actions = self.child.handle_command(command);
        for action in actions.iter() {
            LoggerMiddleware::<T>::log_action(&action)
        }
        actions
    }
}

pub struct Registration<'a> {
    nick: &'a str,
    name: &'a str,
    real_name: &'a str,
    password: Option<&'a str>,
    registered: bool,
}

impl<'a> Registration<'a> {
    pub fn new(nick: &'a str, name: &'a str, real_name: &'a str) -> Registration<'a> {
        Registration{
            nick: nick,
            name: name,
            real_name: real_name,
            password: None,
            registered: false,
        }
    }

    pub fn set_password(&mut self, password: &'a str) -> () {
        self.password = Some(password);
    }

    pub fn is_registered(&self) -> bool {
        self.registered
    }
}

impl<'a> Handler for Registration<'a> {
    fn handle_connect(&mut self) -> Actions {
        let mut res = Vec::new();

        if let Some(pass) = self.password {
            res.push(Action::Reply(Command{
                prefix: None,
                name: "PASS".to_string(),
                args: vec![pass.to_string()]
            }))
        }

        res.push(Action::Reply(Command{
            prefix: None,
            name: "USER".to_string(),
            args: vec![self.name.to_string(), "8".to_string(), "*".to_string(), self.real_name.to_string()],
        }));
        res.push(Action::Reply(Command{
                prefix: None,
                name: "NICK".to_string(),
                args: vec![self.nick.to_string()],
        }));
        res
    }
                
    fn handle_join(&mut self, _channel: &Channel) -> Actions {
        vec![]
    }
            
    fn handle_command(&mut self, command: &Command) -> Actions {
        match command.name.as_str() {
            "001" => self.registered = true,
            _ => ()
        }
        vec![]
    }
}

pub struct ChannelJoin<'a> {
    channels: Vec<&'a str>,
}

impl<'a> ChannelJoin<'a> {
    pub fn new() -> ChannelJoin<'a> {
        ChannelJoin {
            channels: Vec::new(),
        }
    }

    pub fn add(&mut self, channel: &'a str) {
        self.channels.push(channel);
    }
}

impl<'a> Handler for ChannelJoin<'a> {
    fn handle_connect(&mut self) -> Actions {
        vec![]
    }

    fn handle_join(&mut self, _channel: &Channel) -> Actions {
        vec![]
    }

    fn handle_command(&mut self, command: &Command) -> Actions {
        match command.name.as_str() {
            "001" => {
                println!("joining channels!");
                self.channels.iter().map(|ch| {
                    Action::Reply(Command{
                        prefix: None,
                        name: "JOIN".to_string(),
                        args: vec![ch.to_string()],
                    })
                }).collect()
            },
            _ => vec![]
        }
    }
}
