use std::fmt;
use std::str;

#[derive(Debug)]
pub struct User {
    pub nick: String,
    pub ident: String,
    pub host: String,
}

impl fmt::Display for User {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}!{}@{}", self.nick, self.ident, self.host)
    }
}

#[derive(Debug)]
pub struct Channel {
    name: String,
}

impl fmt::Display for Channel {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

#[derive(Debug)]
pub enum Reference {
    User(User),
    Channel(String),
    UserInChannel(User,String),
}

impl fmt::Display for Reference {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Reference::User(ref u) =>
                u.fmt(f),
            Reference::Channel(ref c) =>
                c.fmt(f),
            Reference::UserInChannel(ref u, ref c) =>
                u.fmt(f).and(write!(f, " in ")).and(c.fmt(f)),
        }
    }
}

#[derive(Debug)]
pub struct ParseError {
    pub reason: ParseErrorReason,
}

impl ParseError {
    fn new(reason: ParseErrorReason) -> ParseError {
        ParseError { reason: reason }
    }
}

#[derive(Debug)]
pub enum ParseErrorReason {
    SpaceExpected,
    EmptyName,
    EmptyArg,
}
        
    

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "IRC Parse error")
    }
}

#[derive(Debug,Clone)]
pub struct Command {
    pub prefix: Option<String>,
    pub name: String,
    pub args: Vec<String>,
}



impl fmt::Display for Command {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.prefix.is_some() {
            write!(f, ":{} ", self.prefix.as_ref().unwrap())?
        }
        
        write!(f, "{}", self.name)?;

        for arg in self.args.iter() {
            if self.args.last().map_or(false, |a| a == arg) && arg.contains(' ') {
                write!(f, " :{}", arg)?;
            } else {
                write!(f, " {}", arg);
            }
        }

        Ok(())
    }
}

impl str::FromStr for Command {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let chars : Vec<char> = s.chars().collect();
        let mut i = 0;
        
        let prefix : Option<String> = chars.get(i).and_then(|c| {
            if *c == ':' {
                let mut res = String::new();
                i += 1;
                while chars.get(i).map_or(false, |c| !c.is_whitespace()) {
                    res.push(*chars.get(i).unwrap());
                    i += 1;
                }
                Some(res)
            } else {
                None
            }
        });

        if prefix.is_some() {
            // There must be a space after the prefix
            if !chars.get(i).map_or(false, |c| c.is_whitespace()) { return Err(ParseError::new(ParseErrorReason::SpaceExpected)) }
            i += 1;
        }

        let mut name = String::new();
        while chars.get(i).map_or(false, |c| !c.is_whitespace()) {
            name.push(*chars.get(i).unwrap());
            i += 1;
        }

        // IRC command name cannot be empty
        if name.is_empty() { return Err(ParseError::new(ParseErrorReason::EmptyName)) };

        let mut args = Vec::new();

        while chars.get(i).is_some() {
            match *chars.get(i).unwrap() {
                ' ' =>
                    if args.last().map_or(false, |s: &String| s.is_empty()) {
                        return Err(ParseError::new(ParseErrorReason::EmptyArg))
                    } else {
                        args.push(String::new())
                    }
                ':' => {
                    i += 1;
                    while chars.get(i).is_some() {
                        args.last_mut().ok_or(ParseError::new(ParseErrorReason::SpaceExpected))?.push(*chars.get(i).unwrap());
                        i += 1;
                    }
                },
                c => args.last_mut().ok_or(ParseError::new(ParseErrorReason::SpaceExpected))?.push(c),
            };
            i += 1;
        }
        
        Ok(Command{ prefix: prefix, name: name, args: args })
    }
}

#[derive(Debug)]
pub struct Privmsg {
    pub reference: Reference,
    pub content: String
}

impl Privmsg {
    pub fn from_command(command: &Command) -> Option<Privmsg> {
        if command.name == "PRIVMSG" && command.args.len() == 2 {
            Some(Privmsg{
                reference: Reference::Channel(command.args.get(0).unwrap().to_string()),
                content: command.args.get(1).unwrap().to_string(),
            })
        } else {
            None
        }
    }

    pub fn to_command(&self) -> Command {
        let args = match self.reference {
            Reference::Channel(ref c) => vec![c.to_string(), self.content.to_string()],
            Reference::User(ref u) => vec![u.to_string(), self.content.to_string()],
            Reference::UserInChannel(ref u, ref c) => vec![c.to_string(), format!("{}: {}", u.to_string(), self.content.to_string())]
        };
        Command {
            prefix: None,
            name: "PRIVMSG".to_string(),
            args: args,
        }
    }
}
