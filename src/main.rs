mod irc;
mod handlers;

use std::io::prelude::*;
use std::net;
use std::io;
use std::sync::Arc;

extern crate clap;
extern crate rustls;
extern crate webpki;
extern crate webpki_roots;

use clap::{App,Arg};


fn connect<T: handlers::Handler>(host: &str, port: &str, handler: &mut T) {
    let host_port = format!("{}:{}", host, port);
    let sock = net::TcpStream::connect(host_port).unwrap();

    let mut config = rustls::ClientConfig::new();
    config.root_store.add_server_trust_anchors(&webpki_roots::TLS_SERVER_ROOTS);
    let rc_config = Arc::new(config);
    let kent_cs = webpki::DNSNameRef::try_from_ascii_str(host).unwrap();
    let tls_client = rustls::ClientSession::new(&rc_config, kent_cs);
    let stream = rustls::StreamOwned::new(tls_client, sock);

    let mut reader = io::BufReader::new(stream);

    
    for action in handler.handle_connect().into_iter() {
        write!(reader.get_mut(), "{}\r\n", action.to_command());
    }

    loop {
        let mut line = String::new();
        reader.read_line(&mut line).unwrap();
        let raw = line.trim_right_matches("\r\n");
        let cmd : irc::Command = raw.parse().unwrap();
        let actions = handler.handle_command(&cmd);
        for action in actions.into_iter() {
            write!(reader.get_mut(), "{}\r\n", action.to_command());
        }
        reader.get_mut().flush().unwrap();
    }
}

fn main() {
    let matches = App::new("rustie")
        .version("0.1")
        .author("Joe Harrison <joe@sigwinch.uk>")
        .about("Rust IRC bot")
        .arg(Arg::with_name("HOST")
             .help("Host to connect to")
             .required(true)
             .index(1))
        .arg(Arg::with_name("PORT")
             .help("Port to connect to")
             .default_value("6697")
             .index(2))
        .arg(Arg::with_name("nick")
             .help("nickname to use once connected")
             .short("n")
             .long("nick")
             .default_value("rustie")
             .takes_value(true))
        .arg(Arg::with_name("pass")
             .help("password to use for PASS command")
             .short("p")
             .long("pass")
             .takes_value(true))
        .arg(Arg::with_name("user")
             .help("username to use when connecting")
             .short("u")
             .long("user")
             .default_value("rustie")
             .takes_value(true))
        .arg(Arg::with_name("channel")
             .help("channel to join after connecting")
             .short("c")
             .long("channel")
             .takes_value(true)
             .multiple(true))
        .get_matches();

    let host = matches.value_of("HOST").unwrap();
    let port = matches.value_of("PORT").unwrap();
    
    let mut ping = handlers::Ping::new();

    let nick = matches.value_of("nick").unwrap();
    let user = matches.value_of("user").unwrap();
    let mut reg = handlers::Registration::new(nick, user, "Rustie Bot");

     if let Some(pass) = matches.value_of("pass") {
        reg.set_password(pass);
    }

    let mut chan = handlers::ChannelJoin::new();
    if let Some(chs) = matches.values_of("channel") {
        for ch in chs {
            chan.add(ch);
        }
    }

    let mut dispatcher = handlers::Dispatcher::new();
    dispatcher.register(&mut ping);
    dispatcher.register(&mut reg);
    dispatcher.register(&mut chan);

    let mut client = handlers::LoggerMiddleware::new(dispatcher);

    connect(host, port, &mut client);
}
